// var h1_ = document.querySelector(".titulo");
// console.log(h1_.textContent);
// h1_.textContent = "Aparecida Nutricionista";
// console.log(h1_.textContent);

var h1_ = document.querySelector(".titulo");
h1_.textContent = "Aparecida Nutricionista";

var paciente = document.querySelector("#primeiro-paciente");

var tdPeso = paciente.querySelector(".info-peso");
var tdAltura = paciente.querySelector(".info-altura");
var tdImc = paciente.querySelector(".info-imc");

var peso = tdPeso.textContent;
var altura = tdAltura.textContent;

var alturaEhValida = true;
var pesoEhValido = true;

if (peso <= 0 || peso >= 1000) {
  console.log("Peso inválido!");
  tdPeso.textContent = "Peso inválido!";
  pesoEhValido = false;

}

if (altura <= 0 || altura >= 3) {
  console.log("Altura inválida!");
  tdAltura.textContent = "Altura inválida!";
  alturaEhValida = false;
}

if (alturaEhValida && pesoEhValido) {
  var imc = peso / (altura * altura);
  tdImc.textContent = imc;    
} else {
  tdImc.textContent = "Altura e/ou peso inválidos!"
}